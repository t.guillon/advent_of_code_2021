import pandas as pd
import pathlib
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

dirs = pd.read_csv(datafile, names=['dir', 'steps'], sep=' ')

# Star 1
def move(dirs):
    direction, step = dirs
    if direction=='forward':
        return (step, 0.)
    if direction=='down':
        return (0., step)
    if direction=='up':
        return (0., -step)
        
t0 = time.time()
moves = dirs.apply(
    move,
    axis=1,
    result_type ='expand' # to dispatch into 2 columns rather than 1 tuple of 2
    )
dx,dy = moves.sum().values
print(f'Steps x: {dx}, steps y: {dy}, mult: {dx*dy}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# Star 2
def aim(dirs):
    direction, step = dirs
    if direction=='forward':
        return 0
    if direction=='down':
        return step
    if direction=='up':
        return -step
        
def move(dirs):
    direction, step, aim = dirs
    if direction=='forward':
        return (step, step*aim)
    return (0.,0.)

t0 = time.time()
dirs['aim'] = dirs.apply(aim, axis=1).cumsum()
moves = dirs.apply(
    move,
    axis=1,
    result_type ='expand'
    )
dx,dy = moves.sum().values
print(f'Steps x: {dx}, steps y: {dy}, mult: {dx*dy}')
print(f'(achieved in {time.time()-t0:.2f}s)')