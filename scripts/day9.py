import numpy as np
import pathlib
from PIL import Image, ImageDraw
from scipy.ndimage import label
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'
OUTFOLDER = CWD.parent/'artifacts'/f'{pathlib.Path(__file__).stem}'

heightmap = np.loadtxt(DATAFILE, dtype=str)

# Preprocessing: extracting single values
heightmap = np.array(
    [   [int(val) for val in list(row)]
        for row in heightmap
        ],
    dtype=int
    )

# Star 1
t0 = time.time()
descending_rows = np.diff(heightmap, axis=0) < 0
descending_cols = np.diff(heightmap, axis=1) < 0
low_points = np.ones_like(heightmap, dtype=bool)
low_points [1:] &= descending_rows
low_points [:-1] &= ~descending_rows
low_points [:, 1:] &= descending_cols
low_points [:, :-1] &= ~descending_cols
risk_levels = heightmap[low_points] + 1
print(f'The overall risk level is of {risk_levels.sum()}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# Star 2 - direct approach: a "crawler" spreads through arr to gather neighbors
t0 = time.time()
nrows, ncols = heightmap.shape
size = (nrows+2, ncols+2)
crawled = np.zeros(size, dtype=bool)
basins = np.zeros(size, dtype=int)
crawled[0] = True
crawled[-1] = True
crawled[:,0] = True
crawled[:,-1] = True
stop_val = 9
ibasin = 1
basin_sizes = {}

def img_frombytes(data):
    """ Converting a boolean array into a black and white PIL image
    
    data: np.ndarray of booleans
    
    Returns: PIL Image
    
    See: https://stackoverflow.com/questions/50134468/convert-boolean-numpy-array-to-pillow-image
    """
    size = data.shape[::-1]
    databytes = np.packbits(data, axis=1)
    return Image.frombytes(mode='1', size=size, data=databytes).convert('L')

images = [img_frombytes(basins>0)]
for istart, row in enumerate(basins[1:-1],1):
    for jstart, _ in enumerate(row[1:-1],1):
        indices_to_crawl = [(istart,jstart)]
        nelems_in_basin = 0
        for icrawl, jcrawl in indices_to_crawl:
            if crawled[icrawl,jcrawl]:
                continue
            crawled[icrawl,jcrawl] = True
            val = heightmap[icrawl-1,jcrawl-1]
            if val == stop_val:
                continue
            basins[icrawl,jcrawl] = ibasin
            nelems_in_basin += 1
            new_indices = [
                (icrawl, jcrawl-1),
                (icrawl, jcrawl+1),
                (icrawl-1, jcrawl),
                (icrawl+1, jcrawl)
                ]
            new_indices = [
                (inew,jnew)
                for inew,jnew in new_indices
                if not crawled[inew,jnew]
                ]
            if new_indices:
                indices_to_crawl.extend(new_indices)
        if nelems_in_basin:
            im = img_frombytes(basins>0)
            images.append(im)
            basin_sizes[ibasin] = nelems_in_basin
            ibasin += 1
basin_sizes = sorted(basin_sizes.values())
total_size = 1
for basin_size in basin_sizes[-3:]:
    total_size *= basin_size
print(f'The size multiplication of the 3 largest basins is {total_size}')
print(f'(achieved in {time.time()-t0:.2f}s)')
images[0].save(
    OUTFOLDER/'basin_seeking_main_steps.gif',
    save_all = True, append_images = images[1:], 
    optimize=True, duration = 5
    )

# Star 2 - using scipy.
# Labeling adapts well to scaling of input, but getting sizes does NOT AT ALL
# For default input, is ~10x faster
# For input tiled 100 times, is ~15x slower (labeling only is ~200x faster)
t0 = time.time()
basins_2, nbasins = label(heightmap!=9)
basin_sizes_2 = [(basins==ibasin+1).sum() for ibasin in range(nbasins)]
basin_sizes_2.sort()
total_size_2 = 1
for basin_size in basin_sizes_2[-3:]:
    total_size_2 *= basin_size
print(f'The size multiplication of the 3 largest basins is {total_size_2}')
print(f'(achieved in {time.time()-t0:.2f}s)')
