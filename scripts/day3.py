import math
import numpy as np
import pandas as pd
import pathlib
from scipy.signal import fftconvolve
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

signals = np.loadtxt(
    DATAFILE,
    converters={0: lambda it: int(it, base=2)},
    dtype=int
    )
nsignals = len(signals)
nsignals_half = math.ceil(nsignals/2)

# Star 1
t0 = time.time()
nbits = np.array(np.log2(signals), dtype=int).max() + 1
pows = np.array([2**ibit for ibit in range(nbits)])[::-1]
bits = np.zeros((len(signals), nbits), dtype=int)
bits[:] = (pows & signals.reshape((-1,1))) > 0
ones = bits.sum(axis=0)
gamma = np.dot(ones >= nsignals_half, pows)
espilon = np.dot(ones < nsignals_half, pows)
print(f'Gamma: {gamma}, epsilon: {espilon}, product: {gamma*espilon}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# Star2
t0 = time.time()
gen = np.array(bits)
scrub = np.array(bits)
ngen_half = nsignals_half
nscrub_half = nsignals_half
gen_found = False
scrub_found = False
for ibit in range(nbits):
    if not gen_found:
        keep_ones = gen[:,ibit].sum() >= ngen_half
        gen = gen[gen[:,ibit]==keep_ones]
        ngen_half = math.ceil(len(gen)/2)
        if len(gen)==1:
            gen = np.dot(gen[0], pows)
            gen_found = True
    if not scrub_found:
        keep_ones = scrub[:,ibit].sum() >= nscrub_half
        scrub = scrub[scrub[:,ibit]==(not keep_ones)]
        nscrub_half = math.ceil(len(scrub)/2)
        if len(scrub)==1:
            scrub = np.dot(scrub[0], pows)
            scrub_found = True
    if gen_found & scrub_found:
        break
print(f'Oxygen generator: {gen}, CO2 scrubber: {scrub}, product: {gen*scrub}')
print(f'(achieved in {time.time()-t0:.2f}s)')
