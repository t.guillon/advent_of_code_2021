import numpy as np
import pathlib
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

timers_init = np.loadtxt(
    DATAFILE,
    delimiter=',',
    dtype=int
    )

# Star 1
t0 = time.time()
ndays = 80
new_timer = 8
reset_timer = 6
ntimers = np.zeros((ndays+1, new_timer+1), dtype=int)
ntimers[0,:] = (
    timers_init == np.arange(new_timer+1).reshape((-1,1))
    ).sum(axis=1)
for iday in range(1, ndays+1):
    ntimers[iday,:-1] = ntimers[iday-1, 1:]
    ntimers[iday, [reset_timer,new_timer]] += ntimers[iday-1,0]
print(f'N fishes after {ndays} days: {ntimers[-1].sum()}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# Star 2
t0 = time.time()
ndays = 256
new_timer = 8
reset_timer = 6
ntimers = np.zeros(
    (ndays+1, new_timer+1),
    dtype=np.int64
    ) # default np.int32 max limit reached otherwise
ntimers[0,:] = (
    timers_init == np.arange(new_timer+1).reshape((-1,1))
    ).sum(axis=1)
for iday in range(1, ndays+1):
    ntimers[iday,:-1] = ntimers[iday-1, 1:]
    ntimers[iday, [reset_timer,new_timer]] += ntimers[iday-1,0]
print(f'N fishes after {ndays} days: {ntimers[-1].sum()}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# # Star 2: just for fun:  comparing with naive looping
# # Naive version is slightly faster (~10% according to timeit)
# t0 = time.time()
# ndays = 256
# new_timer = 8
# reset_timer = 6
# ntimers = [0]*(new_timer+1)
# init_list = list(timers_init)
# for itimer in range(new_timer+1):
    # ntimers[itimer] = init_list.count(itimer)
# for iday in range(1, ndays+1):
    # nnew_fishes = ntimers[0]
    # for itimer in range(1, new_timer+1):
        # ntimers[itimer-1] = ntimers[itimer]
    # ntimers[reset_timer] += nnew_fishes
    # ntimers[new_timer] = nnew_fishes
# print(f'N fishes after {ndays} days: {sum(ntimers)}')
# print(f'(achieved in {time.time()-t0:.2f}s)')
