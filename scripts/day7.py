import numpy as np
import pathlib
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

positions = np.loadtxt(
    DATAFILE,
    delimiter=',',
    dtype=int
    )

# Preprocessing: find possible positions (in case they do not start at 0)
possible_positions = np.arange(positions.min(), positions.max()+1)

# Star 1
t0 = time.time()
moves = np.abs(positions - possible_positions.reshape((-1,1)))
burned_fuel = moves.sum(axis=1)
least_burned_fuel = burned_fuel.min()
least_position = np.argwhere(burned_fuel==least_burned_fuel).flatten()
print(f'Position {least_position} burns less fuel: {least_burned_fuel} fuel')
print(f'(achieved in {time.time()-t0:.2f}s)')

# Star 2
t0 = time.time()
def burned_fuel_on_move(nsteps):
    """ Total fuel burned on a move of nsteps steps """
    return (nsteps*(nsteps+1))//2
burned_on_move_arr = burned_fuel_on_move(possible_positions)
moves = np.abs(positions - possible_positions.reshape((-1,1)))
burned_fuel = burned_on_move_arr[moves.flat].reshape(moves.shape).sum(axis=1)
# First try below is way slower (e.g. 30% slower with 500k elements)
# burned_fuel = burned_fuel_on_move(moves).sum(axis=1)
least_burned_fuel = burned_fuel.min()
least_position = np.argwhere(burned_fuel==least_burned_fuel).flatten()
print(f'Position {least_position} burns less fuel: {least_burned_fuel} fuel')
print(f'(achieved in {time.time()-t0:.2f}s)')
