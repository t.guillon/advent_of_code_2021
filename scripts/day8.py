import numpy as np
import pandas as pd
import pathlib
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

ninputs = 10
noutputs = 4
names_in = [f'input {i+1}' for i in range(ninputs)]
names_out = [f'output {i+1}' for i in range(noutputs)]
segments = pd.read_csv(
    DATAFILE,
    names=names_in+names_out,
    sep=' \| | ',
    engine='python'
    )

# Star 1
t0 = time.time()
nsegments_per_digit = [6, 2, 5, 5, 4, 5, 6, 3, 7, 6]
nsegments = segments.apply(lambda col: col.str.len())
easy_digits = [1, 4, 7, 8]
ndigits_easy = 0
for easy_digit in easy_digits:
    is_easy_digit = nsegments[names_out]==nsegments_per_digit[easy_digit]
    ndigits_easy += is_easy_digit.values.sum()
print(f'Number of {easy_digits} = {ndigits_easy}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# Star 2: direct approach using sets
t0 = time.time()
def get_output(inputs, outputs):
    """ Decoding the 4-digit output from the input combination
    
    See pseudo-algorithm below
    
    inputs: list of 10 str
        Each element depicts the wire combination of one of 0-9
        Wire combinations are str of 'a', 'b', 'c', 'd', 'e', 'f' or 'g'
        See AdventOfCode page for more details
    outputs: list of 4 str
        Wire combinations for each of the output digits
        
    Returns: int
        The decoded output number of 4 digits
        
    Pseudo-algo:
    - find easy digits from combination lengths:
      - len(combi)2->1,
      - len(combi)4->4,
      - len(combi)3->7,
      - len(combi)7->8
    - find combination candidates for other digits:
      - len(combi)5->2 OR 3 OR 5,
      - len(combi)6->0 OR 6 OR 9,
    - letter in 1 -> c/f
    - letter in 1 AND not in 7 -> a 
    - letter in 4 AND not in 1 -> b/d
    - ndigit=5 AND has b AND has d -> 5 
    - letter in 1 AND not in 5 -> c
    - letter in 1 AND not c -> f
    - letter in 5 AND not in [a,b/d,c,f] -> g
    - ndigit=5 AND has f -> 3
    - letter in 3 AND not in [a,c,f,g] -> d
    - letter in 4 AND not d -> b    
    - letter in 8 AND not in [a,b,c,d,f,g] -> e
    - a, b, c, d, e, f, g decyphered
      -> digits and wire combinations can be paired
      -> find output digits from their combinations
        
    """
    nsegments_per_digit = [6, 2, 5, 5, 4, 5, 6, 3, 7, 6]
    inputs = [set(digit) for digit in inputs]
    outputs = [set(digit) for digit in outputs]
    # segs_candidates[i] = possible wire combinations for digit i
    segs_candidates = [
        list(filter(lambda item: len(item)==nsegments_per_digit[digit], inputs))
        for digit in range(10)
        ]
    segs_candidates = [
        seg.pop()
        if len(seg)==1
        else seg
        for seg in segs_candidates
        ]
    # segs[i] = wire combinations for digit i, updated below
    segs = [seg.copy() for seg in segs_candidates]
    a = segs[7].difference(segs[1]).pop()
    bd = segs[4].difference(segs[1])
    for iseg, seg in enumerate(segs_candidates[5]):
        if seg.issuperset(bd):
            segs[5] = segs_candidates[5].pop(iseg)
    c = segs[1].difference(segs[5]).pop()
    f = segs[1].difference(c).pop()
    g = segs[5].difference(bd.union(a,c,f)).pop()
    for iseg, seg in enumerate(segs_candidates[5]):
        if seg.issuperset(f):
            segs[3] = segs_candidates[5].pop(iseg)
            segs[2] = segs_candidates[5].pop()
    d = segs[3].difference((a,c,f,g)).pop()
    b = bd.difference(d).pop()
    e = set('abcdefg').difference(a,b,c,d,f,g).pop()
    segs[0] = set((a,b,c,e,f,g))
    segs[6] = set((a,b,d,e,f,g))
    segs[9] = set((a,b,c,d,f,g))
    output = [
        fact*segs.index(out)
        for out,fact in zip(outputs, (1000, 100, 10, 1))
        ]
    return sum(output)
outputs = segments.apply(
    lambda row: get_output(row[names_in], row[names_out]),
    axis=1
    )
print(f'The sum of outputs is {outputs.sum()}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# Star 2: speeding up by using bitwise logic and vectorization
# -> ~70x speed up with segments repeated 1000 times: pd.concat([segments]*1000)
t0 = time.time()
nconsoles = len(segments)
encoding = dict((char, 2**ichar) for ichar, char in enumerate('abcdefg'))
def wire_to_int(wire, encoding=encoding):
    value = 0
    for char in wire:
        value += encoding[char]
    return value
t0 = time.time()
wires_in = segments.apply(
    lambda col: col.apply(
        lambda item: wire_to_int(item)
        )
    )
wires_out = wires_in[names_out].values
wires_in = wires_in[names_in].values
digits_in = np.zeros_like(wires_in, dtype=np.int8)
# digits_out = wires_in[:,:,np.newaxis] == wires_out[:,np.newaxis,:]
# digits_out
# data = np.zeros_like(segments, dtype=np.int8)
# data[:,:] = segments.apply(
    # lambda col: col.apply(
        # lambda item: wire_to_int(item)
        # )
    # )
# print(time.time()-t0)
# tt=time.time()
nwires_in = np.zeros_like(wires_in, dtype=np.int8)
nwires_in[:,:] = segments[names_in].apply(
    lambda col: col.apply(
        lambda item: len(item)
        )
    )
digits_in[:,1] = wires_in[nwires_in==2]
digits_in[:,4] = wires_in[nwires_in==4]
digits_in[:,7] = wires_in[nwires_in==3]
digits_in[:,8] = wires_in[nwires_in==7]
cf = digits_in[:,1]
a = digits_in[:,1] ^ digits_in[:,7]
bd = digits_in[:,1] ^ digits_in[:,4]
wires_in_235_candidates = wires_in[nwires_in==5].reshape((nconsoles,3))
digits_in[:,5] = wires_in_235_candidates[
    (   wires_in_235_candidates
        & bd[:,np.newaxis]
        ) == bd[:,np.newaxis]
    ]
bdcf = bd | cf
wires_in_069_candidates = wires_in[nwires_in==6].reshape((nconsoles,3))
digits_in[:,9] = wires_in_069_candidates[
    (   wires_in_069_candidates
        & bdcf[:,np.newaxis]
        )==(
        bdcf[:,np.newaxis]
        )
    ]
c = digits_in[:,5] ^ digits_in[:,9]
f = cf ^ c
g = digits_in[:,9] ^ a ^ digits_in[:,4]
digits_in[:,3] = wires_in_235_candidates[
    (   wires_in_235_candidates
        & cf[:,np.newaxis]
        ) == cf[:,np.newaxis]
    ]
acfg = a | c | f | g
d = digits_in[:,3] ^ acfg
b = bd ^ d
e = digits_in[:,8] ^ digits_in[:,9]
digits_in[:,6] = digits_in[:,5] | e
digits_in[:,0] = digits_in[:,8] ^ d
digits_in[:,2] = a | c | d | e | g
digits_out = digits_in[:,np.newaxis,:]==wires_out[:,:,np.newaxis]
digits_out = (
    np.argwhere(digits_out[:,0,:]).T[1]*1000
    + np.argwhere(digits_out[:,1,:]).T[1]*100
    + np.argwhere(digits_out[:,2,:]).T[1]*10
    + np.argwhere(digits_out[:,3,:]).T[1]
    )
print(f'The sum of outputs is {digits_out.sum()}')
print(f'(achieved in {time.time()-t0:.2f}s)')