import numpy as np
import pandas as pd
import pathlib
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

nav_sub = pd.read_csv(
    DATAFILE,
    names=['syntax'],
    converters={'syntax': lambda string: string.strip('\n')}
    )

# Preprocessing

# 
# Star 1
t0 = time.time()
# nlines = len(nav_sub)
nav_sub_count = nav_sub.copy()
nav_sub_count['parentheses'] = 0
nav_sub_count['brackets'] = 0
nav_sub_count['curly_brackets'] = 0
nav_sub_count['chevrons'] = 0

# parentheses, brackets, curly brackets, chevrons
# descending_rows = np.diff(heightmap, axis=0) < 0
# descending_cols = np.diff(heightmap, axis=1) < 0
# low_points = np.ones_like(heightmap, dtype=bool)
# low_points [1:] &= descending_rows
# low_points [:-1] &= ~descending_rows
# low_points [:, 1:] &= descending_cols
# low_points [:, :-1] &= ~descending_cols
# risk_levels = heightmap[low_points] + 1
# syntax_error_score
print(f'The total syntax error score is {syntax_error_score}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# # Star 2 - direct approach: a "crawler" spreads through arr to gather neighbors
# # Woops: something is buggy. Check later
# t0 = time.time()
# crawled = np.zeros_like(heightmap, dtype=bool)
# basins = np.zeros_like(heightmap, dtype=np.int8)
# nrows,ncols = basins.shape
# stop_val = 9
# ibasin = 1
# basin_sizes = {}
# for istart, row in enumerate(basins):
    # for jstart, val in enumerate(row):
        # indices_to_crawl = [(istart,jstart)]
        # nelems_in_basin = 0
        # for icrawl, jcrawl in indices_to_crawl:
            # if crawled[icrawl,jcrawl]:
                # continue
            # crawled[icrawl,jcrawl] = True
            # val = heightmap[icrawl,jcrawl]
            # if val == stop_val:
                # continue
            # basins[icrawl,jcrawl] = ibasin
            # nelems_in_basin += 1
            # new_indices = [
                # (icrawl, jcrawl-1, jcrawl>0),
                # (icrawl, jcrawl+1, jcrawl<ncols-1),
                # (icrawl-1, jcrawl, icrawl>0),
                # (icrawl+1, jcrawl, icrawl<nrows-1)
                # ]
            # new_indices = [
                # (inew,jnew)
                # for inew,jnew,cond in new_indices
                # if cond and not crawled[inew,jnew]
                # ]
            # if new_indices:
                # indices_to_crawl.extend(new_indices)
        # if nelems_in_basin:
            # basin_sizes[ibasin] = nelems_in_basin
            # ibasin += 1
# basin_sizes = sorted(basin_sizes.values())
# total_size = 1
# for basin_size in basin_sizes[-3:]:
    # total_size *= basin_size
# print(f'The size multiplication of the 3 largest basins is {total_size}')
# print(f'(achieved in {time.time()-t0:.2f}s)')

# # Star 2 - using scipy.
# # Labeling adapts well to scaling of input, but getting sizes does NOT AT ALL
# # For default input, is ~10x faster
# # For input tiled 100 times, is ~15x slower (labeling only is ~200x faster)
# t0 = time.time()
# basins_2, nbasins = label(heightmap!=9)
# basin_sizes_2 = [(basins==ibasin+1).sum() for ibasin in range(nbasins)]
# basin_sizes_2.sort()
# total_size_2 = 1
# for basin_size in basin_sizes_2[-3:]:
    # total_size_2 *= basin_size
# print(f'The size multiplication of the 3 largest basins is {total_size_2}')
# print(f'(achieved in {time.time()-t0:.2f}s)')
