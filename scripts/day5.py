import numpy as np
import pandas as pd
import pathlib
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

moves = pd.read_csv(
    DATAFILE,
    names=['x0', 'y0', 'x1', 'y1'],
    sep=',| -> ',
    engine='python'
    )

# Preprocessing: move to a grid starting at (0,0)
mins = moves.min(axis=0)
maxs = moves.max(axis=0)
xmin = mins[::2].min()
xmax = maxs[::2].min()
ymin = mins[1::2].min()
ymax = maxs[1::2].min()
moves -= (xmin, ymin, xmin, ymin)
ncols = xmax-xmin+1
nrows = ymax-ymin+1

# Star 1
t0 = time.time()
visited_points = np.zeros((nrows, ncols), dtype=int)
moves['Dx'] = moves['x1']-moves['x0']
moves['Dy'] = moves['y1']-moves['y0']
moves['straight'] = np.logical_or(moves['Dx']==0, moves['Dy']==0)
moves_straight = moves[moves['straight']]
moves_x = moves_straight[moves_straight['Dx']!=0][['x0', 'x1', 'y0']].values
moves_y = moves_straight[moves_straight['Dy']!=0][['y0', 'y1', 'x0']].values
moves_x[:,:2].sort(axis=1)
moves_y[:,:2].sort(axis=1)
for istart, iend, jcol in moves_y:
    visited_points[istart:iend+1, jcol] += 1
for jstart, jend, irow in moves_x:
    visited_points[irow, jstart:jend+1] += 1
print(f'N points visited at least twice: {(visited_points >= 2).sum()}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# Star2
t0 = time.time()
visited_points = np.zeros((nrows, ncols), dtype=int)
moves['Dx'] = moves['x1']-moves['x0']
moves['Dy'] = moves['y1']-moves['y0']
moves['straight'] = np.logical_or(moves['Dx']==0, moves['Dy']==0)
moves_straight = moves[moves['straight']]
moves_x = moves_straight[moves_straight['Dx']!=0][['x0', 'x1', 'y0']].values
moves_y = moves_straight[moves_straight['Dy']!=0][['y0', 'y1', 'x0']].values
moves_x[:,:2].sort(axis=1)
moves_y[:,:2].sort(axis=1)
for istart, iend, jcol in moves_y:
    visited_points[istart:iend+1, jcol] += 1
for jstart, jend, irow in moves_x:
    visited_points[irow, jstart:jend+1] += 1
moves['diagonal'] = moves['Dx'].abs() == moves['Dy'].abs()
moves_diagonal = moves[moves['diagonal']][['x0', 'x1', 'y0', 'y1']].values
moves_left = np.diff(moves_diagonal[:,:2], axis=1) < 0
moves_up = np.diff(moves_diagonal[:,2:], axis=1) < 0
moves_diagonal[:,:2].sort(axis=1)
moves_diagonal[:,2:].sort(axis=1)
for indices, move_left, move_up in zip(moves_diagonal, moves_left, moves_up):
    jstart, jend, istart, iend = indices
    pts_view = visited_points[istart:iend+1, jstart:jend+1]
    if move_left:
        pts_view = np.fliplr(pts_view)
    if move_up:
        pts_view = np.flipud(pts_view)
    pts_view[np.diag_indices_from(pts_view)] += 1
# # Quite surprisingly, operations below are slower although more vectorized
# visited_points_ud_view = np.flipud(visited_points)
# visited_points_lr_view = np.fliplr(visited_points)
# visited_points_ud_lr_view = np.fliplr(visited_points_ud_view)
# disps = np.diff(moves_diagonal[:,:2], axis=1).flatten()
# for indices, move_left, move_up, disp in zip(moves_diagonal, moves_left, moves_up, disps):
    # jstart, jend, istart, iend = indices
    # if move_left:
        # if move_up:
            # pts_view = visited_points_ud_lr_view
        # else:
            # pts_view = visited_points_lr_view
    # elif move_up:
        # pts_view = visited_points_ud_view
    # else:
        # pts_view = visited_points
    # pts_view[istart:iend+1,jstart:jend+1].flat[::disp+1] += 1
print(f'N points visited at least twice: {(visited_points >= 2).sum()}')
print(f'(achieved in {time.time()-t0:.2f}s)')
