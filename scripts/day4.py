import numpy as np
import pathlib
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

boards = np.loadtxt(
    DATAFILE,
    skiprows=2,
    dtype=int
    ).reshape((-1, 5, 5))
draws = np.loadtxt(
    DATAFILE,
    max_rows=1,
    delimiter=',',
    dtype=int
    )
nboards, nrows, ncols = boards.shape

# Star 1
t0 = time.time()
drawn = np.zeros_like(boards, dtype=bool)
completed_rows = np.zeros((nboards, nrows), dtype=bool)
completed_cols = np.zeros((nboards, ncols), dtype=bool)
iboard = None
irow = None
icol = None
for draw in draws:
    drawn[boards==draw] = True
    completed_rows[:] = drawn.all(axis=2)
    if completed_rows.any():
        iboard, irow = np.argwhere(completed_rows)[0]
        break
    completed_cols[:] = drawn.all(axis=1)
    if completed_cols.any():
        iboard, icol = np.argwhere(completed_cols)[0]
        break
undrawn_sum = boards[iboard][~drawn[iboard]].sum()
print(f'Sum: {undrawn_sum}, drawn: {draw}, product: {undrawn_sum*draw}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# Star2
t0 = time.time()
drawn = np.zeros_like(boards, dtype=bool)
completing_rows = np.zeros((nboards, nrows), dtype=bool)
completing_cols = np.zeros((nboards, ncols), dtype=bool)
completing_boards = np.zeros(nboards, dtype=bool)
newly_completing_boards = np.zeros(nboards, dtype=bool)
completed_boards = np.zeros(nboards, dtype=bool)
iboard = None
for draw in draws:
    drawn[boards==draw] = True
    at_least_one_board_wins = False
    
    completing_rows[:] = drawn.all(axis=2)
    completing_cols[:] = drawn.all(axis=1)
    completing_boards[:] = (
        completing_rows.any(axis=1)
        | completing_cols.any(axis=1)
        )
    newly_completing_boards[:] = completing_boards & ~completed_boards
    at_least_one_new_completion = newly_completing_boards.any()
    if at_least_one_new_completion:
        iboard = np.argwhere(newly_completing_boards).flatten()
        completed_boards[iboard] = True
    all_boards_won = completed_boards.all()
    if all_boards_won:
        break
else:
    raise Exception('Well, some boards cannot win')
undrawn_sum = boards[iboard][~drawn[iboard]].sum()
print(f'Sum: {undrawn_sum}, drawn: {draw}, product: {undrawn_sum*draw}')
print(f'(achieved in {time.time()-t0:.2f}s)')
