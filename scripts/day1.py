import numpy as np
import pathlib
from scipy.signal import fftconvolve
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

depths = np.loadtxt(DATAFILE, dtype=int)

# Star 1
t0 = time.time()
ddepths = np.diff(depths)
nincreases = (ddepths > 0).sum()
print(f'N increases: {nincreases}/{len(ddepths)}')
print(f'(achieved in {time.time()-t0:.2f}s)')

# Star2
ndepths = len(depths)
# - solution 1
# Really fast way of computing cumulative sum
# [depths[j:j+3].sum() for j in range(ndepths-3+1)]
# hum, problem: does not give same result because fftconvolve is
# subjected to float precision
# sliding_means = fftconvolve(depths, np.ones(3, dtype=int), mode='valid')
# t0 = time.time()
# dmeans = np.diff(sliding_means)
# nincreases = (dmeans > 0).sum()
# print(f'N increases: {nincreases}/{len(sliding_means)}')
# print(f'(achieved in {time.time()-t0:.2f}s)')
# - solution 2 : naive version
# t0 = time.time()
# foo = np.array([depths[j:j+3].sum() for j in range(ndepths-3+1)])
# foo = np.diff(foo)
# print(f'N increases: {(foo>0).sum()}/{len(foo)}')
# print(f'(achieved in {time.time()-t0:.2f}s)')
# - solution 3 : more optimized
t0 = time.time()
bar = np.vstack((depths[:-2], depths[1:-1], depths[2:]))
bar = np.diff(bar.sum(axis=0))
print(f'N increases: {(bar>0).sum()}/{len(bar)}')
print(f'(achieved in {time.time()-t0:.2f}s)')