# advent_of_code_2021

Attempts to complete AoC 2021

Go and try these amazing challenges yourself!
https://adventofcode.com/2021

## Content
* artifacts: outputs from some of the puzzles
* data: puzzle inputs
* scripts: the puzzle-solving Python scripts

## License
Well, nothing exceptional here, do whatever you want with it if it can be of any help :).
